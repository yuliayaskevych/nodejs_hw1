const express = require('express');
const app = express();
const fs = require('fs');
const path = require('path');

const PORT = 8080;
const filesPath = '/api/files/';
const dirName = path.join(__dirname, filesPath);

const mimeType = {
    '.json': 'application/json',
    '.xml': 'text/xml',
    '.txt': 'text/plain',
    '.yaml': 'text/yaml',
    '.log': 'text/plain',
    '.js': 'text/javascript'
};

app.use(express.json());

app.get('/', (req, res) => {
    res.send(`<h2>Server listening on port ${PORT}</h2>`);
});

app.post(filesPath, (req, res) => {
    try {
        if (req.body.content && mimeType[path.extname(req.body.filename)]) {
            fs.writeFile(`${dirName}${req.body.filename}`, req.body.content, () => {
                res.status(200);
                res.json({ message: 'File created successfully' });
            });
        } else {
            throw new Error();
        }
    } catch (error) {
        res.status(400).json({ message: "Please specify 'content' parameter" })
    }
});

app.get(filesPath, (req, res) => {
    try {
        fs.readdir(dirName, (err, files) => {
            res.status(200);
            res.json({ message: 'Success', files })
        })
    } catch (err) {
        res.status(400).json({ message: 'Client error' })
    }
});

app.get(`${filesPath}:filename`, (req, res) => {
    try {
        const fileName = req.params.filename;
        let result = {
            message: 'Success',
            filename: fileName,
            content: '',
            extension: '',
            uploadedDate: ''
        };

        result.extension = path.parse(dirName + fileName).ext.slice(1);
        result.uploadedDate = fs.statSync(dirName + fileName).birthtime;

        fs.readFile(dirName + fileName, 'utf8', (err, data) => {
            if (err) {
                throw new Error();
            }
            result.content = data;
            res.status(200);
            res.json(result);
        })
    } catch (error) {
        res.status(400).json({ message: `No file with '${req.params.filename}' filename found` })
    }
});

app.use((req, res) => {
    res.status(500).json({ message: 'Server error' })
});

app.listen(PORT, function () {
    console.log(`Server listening on port ${PORT}`);
});


// const express = require("express");
// const app = express();
// const fs = require("fs");
// const path = require("path");

// const PORT = 8080;
// const filesPath = '/api/files/';
// const dirName = path.join(__dirname, filesPath);

// const mimeType = {
//     '.json': 'application/json',
//     '.xml': 'text/xml',
//     '.txt': 'text/plain',
//     '.yaml': 'text/yaml',
//     '.log': 'text/plain',
//     '.js': 'text/javascript',
// };

// app.use(express.json());

// app.get('/', (req, res) => {
//     res.send(`<h2>Server listening on port ${PORT}</h2>`);
// });

// app.post(filesPath, (req, res) => {
//     try {
//         if (req.body.content && mimeType[path.extname(req.body.filename)]) {
//             fs.writeFile(`dirName${req.body.filename}`, req.body.content, () => {
//                 res.json({ message: "File created successfully" });
//             });
//         } else {
//             throw new Error();
//         }
//     } catch (error) {
//         res.status(400).json({ message: "Please specify 'content' parameter" })
//     }
// });

// app.get(filesPath, (req, res) => {
//     try {
//         fs.readdir(dirName, (err, files) => {
//             res.json({ message: "Successful", files })
//         })
//     } catch (err) {
//         res.status(400).json({ message: "Client error" })
//     }
// });

// app.get(`${filesPath}:filename`, (req, res) => {
//     try {
//         const fileName = req.params.filename;
//         let result = {
//             message: "Success",
//             filename: fileName,
//             content: "",
//             extension: "",
//             uploadedDate: "",
//         };

//         result.extension = path.parse(dirName + fileName).ext.slice(1);
//         result.uploadedDate = fs.statSync(dirName + fileName).birthtime;

//         fs.readFile(dirName + fileName, "utf8", (err, data) => {
//             if (err) throw error;
//             result.content = data
//             res.json(result)
//         })
//     } catch (error) {
//         res.status(400).json({ message: `No file with '${req.params.filename}' filename found` })
//     }
// });

// app.use((req, res) => {
//     res.status(500).json({ message: "Server error" })
// });

// app.listen(PORT, function () {
//     console.log(`Server listening on port ${PORT}`);
// });

